
InVEST freshwater model sample data information and sources
-----------------------------------------------------------

These sample data are from Kenya, where the Natural Capital Project worked with partners to inform the creation of a water fund for the city of Nairobi. 
It is a small sub-watershed called Gura, within the larger area covered by the project. Below are brief notes on the sources of these data. 
Please note that these data are provided as an example only, to help with learning InVEST freshwater models. 
They should not be treated as authoritative, nor used as-is within your own projects.   

See the InVEST User Guide for more information about this tool and its data requirements: http://releases.naturalcapitalproject.org/invest-userguide/latest/


Digital elevation model - DEM_gura.tif: NASA Shuttle Radar Data Topography Mission (SRTM) 30m elevation data, resampled to 15m.

Outlet points - outlet_points.shp: Created for example purposes.

Threshold flow accumulation: A value of 1000 works well.

Pixel distance to snap outlet points: These outlets are very near the streams, so use a small value, like 2 or 3.


